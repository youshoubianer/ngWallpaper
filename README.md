
# ngWallpaper

爬取国家地理每日一图，并设置为桌面壁纸。win10下可行

## Usage
    1. npm install windows-build-tools -g
    2. npm install
    3. npm start
    
    说明：
    windows-build-tools 会安装 python2.7.13，以及 MS C++ 2015 Build tools
    node-gyp 也是必须的，用来编译C++

## 爬取National Geographic每日一图
 
    在国家地理网站中发现了每日一图的配置文件(每天更新，以 YYYY-MM 命名)
    
    配置文件地址：
    http://www.nationalgeographic.com/photography/photo-of-the-day/_jcr_content/.gallery.2017-09.json
    
    根据配置文件下载当日的每日一图

## 设置win10桌面背景

**1. 修改注册表**
> 用 winreg 库修改 HKCU\\control panel\\desktop

```js
var wallpaperReg = new Registry({
    hive: Registry.HKCU, // HKEY_CURRENT_USER
    key: '\\control panel\\desktop'
});

/*
 * key: wallpaper
 * type: REF_SZ
 * value: absolutePath (必须是bmp图像)
 */
wallpaperReg.set('wallpaper', 'REG_SZ', absolutePath, function (error) {})
```
参考：
[设置桌面背景注册表项说明](https://technet.microsoft.com/en-us/library/cc978625.aspx)

**2. 更新设置**
> 调用user32.dll里的 SystemParametersInfoW 方法
> node 下用ffi库设置调用dll

```js
var FFI = require('ffi');
var user32 = new FFI.Library('user32', {
    'SystemParametersInfoW': [
        'int32', ['int32', 'int32', 'string', 'int32']
    ],
});

var SPI_SETDESKWALLPAPER = 0x0014;  // SPI_SETDESKWALLPAPER
var rest = user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, null, 1);
if (rest) {
    return resolve()
} else {
    return reject(new Error('set error:' + rest));
}
```

参考：  
[user32.dll下的系统function介绍](https://www.win7dll.info/user32_dll.html)  
[SystemParametersInfo 函数说明](https://msdn.microsoft.com/en-us/library/windows/desktop/ms724947(v=vs.85).aspx)  
[SystemParametersInfo 函数使用示例](http://www.pinvoke.net/default.aspx/user32.systemparametersinfo)  

## pkgs
- [windows-build-tools](https://github.com/felixrieseberg/windows-build-tools)
- [winreg](https://github.com/fresc81/node-winreg)
- [ffi](https://github.com/node-ffi/node-ffi)

## Todo: Gnome下桌面设置
linux: http://www.linuxsong.org/2010/09/gnome-change-wallpaper-with-command-line/

## 参考

1. https://social.technet.microsoft.com/Forums/windows/en-US/72a9b4bf-071b-47cd-877d-0c0629a9eb90/how-change-the-wallpaperbackground-with-a-command-line-?forum=w7itproui

## 项目参考
https://github.com/genzj/pybingwallpaper