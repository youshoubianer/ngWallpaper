'use strict';
var moment = require('moment');
var request = require('request');
var fs = require('fs');
var path = require('path');
var Registry = require('winreg');
var FFI = require('ffi');

var user32 = new FFI.Library('user32', {
    'SystemParametersInfoW': [
        'int32', ['int32', 'int32', 'string', 'int32']
    ],
});

var wallpaperReg = new Registry({
    hive: Registry.HKCU,
    key: '\\control panel\\desktop'
});

var galleryPath = `http://www.nationalgeographic.com/photography/photo-of-the-day/_jcr_content/.gallery.${moment().format('YYYY-MM')}.json`;
var wallpaperPath = './images/ng-wallpaper.bmp';

// 获取当月配置文件
var getGalleryFile = function () {
    return new Promise(function (resolve, reject) {
        request(galleryPath, function (error, resp, body) {
            if (error) {
                throw new Error(error);
            }
            body = JSON.parse(body);
            fs.writeFileSync(`./gallery.${moment().format('YYYY-MM')}.json`, JSON.stringify(body, '', 2), 'utf8');
            return resolve(body);
        });
    })
}

// 下载今日图片
var getTodayImage = function (imageUrl, publishDate) {
    return new Promise(function (resolve, reject) {
        request(imageUrl, {
            encoding: null,
        }, function (error, resp, body) {
            if (error) {
                throw new Error(error);
            }
            var ext = resp.headers['content-type'].split('/')[1];

            if(!fs.existsSync('./images')){
                fs.mkdirSync('./images');
            }

            fs.writeFileSync(`./images/ng-${moment(new Date(publishDate)).format('YYYY-MM-DD')}.${ext}`, body, 'binary');
            fs.writeFileSync(wallpaperPath, body, 'binary');
            return resolve(path.resolve(__dirname, wallpaperPath));
        });
    });
}

// 设置桌面背景
var setWallpaper = function (absolutePath) {
    return new Promise(function (resolve, reject) {
        wallpaperReg.set('wallpaper', 'REG_SZ', absolutePath, function (error) {
            if (error) {
                console.log(error);
                return reject(error);
            }
            var SPI_SETDESKWALLPAPER = 0x0014;
            var rest = user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, null, 1);
            if (rest) {
                return resolve()
            } else {
                return reject(new Error('set error:' + rest));
            }
        });
    });
}

// 入口
getGalleryFile().then((galleryCfg) => {
    var todayCfg = galleryCfg.items[0];
    var imageUrl = `${todayCfg.url}${todayCfg.originalUrl}`;
    return getTodayImage(imageUrl, todayCfg.publishDate);
}).then((absolutePath) => {
    absolutePath = absolutePath.toLowerCase();
    return setWallpaper(absolutePath);
}).then(function () {
    console.log('success!');
}).catch(error => {
    console.log(error.message)
});


/**
 * .bat
 * 
 * reg add "HKEY_CURRENT_USER\control panel\desktop" /v wallpaper /t REG_SZ /d "" /f 
 * reg add "HKEY_CURRENT_USER\control panel\desktop" /v wallpaper /t REG_SZ /d C:\Users\Administrator\MyBingWallpapers\wallpaper.bmp /f
 * reg add "HKEY_CURRENT_USER\\control panel\\desktop" /v wallpaperBackup /t REG_SZ /d C:\Users\Administrator\MyBingWallpapers\wallpaper.bmp /f`
 * reg add "HKEY_CURRENT_USER\control panel\desktop" /v WallpaperStyle /t REG_SZ /d 10 /f
 * reg add "HKEY_CURRENT_USER\control panel\desktop" /v TileWallpaper /t REG_SZ /d 0 /f`
 * RUNDLL32.EXE user32.dll,UpdatePerUserSystemParameters 
 * pause
 * exit
 */